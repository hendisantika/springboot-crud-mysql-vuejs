# Spring Boot CRUD MySQL VueJS

What you'll need

Your local computer should have JDK 8+ or OpenJDK 8+, Maven 3+, MySQL Server 5+ or Docker CE 18+

## You should also walk through the following tutorials

1. Spring Boot and FreeMarker
2. JPA and Hibernate
3. Lombok in Java and Spring Boot
4. Using Axios to consume REST APIs

## Things to do:
1. Clone this repository: `git clone https://gitlab.com/hendisantika/springboot-crud-mysql-vuejs.git`
2. Go to the folder: `cd springboot-crud-mysql-vuejs`
3. Run the application: `mvn clean spring-boot:run`
4. Go to your favorite browser: http://localhost:8080

## Screenshot

### Index Page

![Index Page](img/home.png "Index Page")

### Add Product Page

![Add Product Page](img/add.png "Add Product Page")

### Edit Product Page

![Edit Product Page](img/edit.png "Edit Product Page")

### List Product Page

![List Product Page](img/list.png "List Product Page")

### Delete Product Page

![Delete Product Page](img/delete.png "Delete Product Page")
