package com.hendisantika.springbootcrudmysqlvuejs.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-crud-mysql-vuejs
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/11/19
 * Time: 22.01
 */
@Controller
public class ProductController {
    @GetMapping("/")
    public String list() {
        return "products";
    }
}