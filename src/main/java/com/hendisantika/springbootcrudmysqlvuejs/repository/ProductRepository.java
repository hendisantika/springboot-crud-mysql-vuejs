package com.hendisantika.springbootcrudmysqlvuejs.repository;

import com.hendisantika.springbootcrudmysqlvuejs.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-crud-mysql-vuejs
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/11/19
 * Time: 21.54
 */
public interface ProductRepository extends JpaRepository<Product, Long> {
}