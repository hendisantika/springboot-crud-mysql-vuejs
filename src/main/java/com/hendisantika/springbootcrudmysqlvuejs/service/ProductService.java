package com.hendisantika.springbootcrudmysqlvuejs.service;

import com.hendisantika.springbootcrudmysqlvuejs.domain.Product;
import com.hendisantika.springbootcrudmysqlvuejs.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-crud-mysql-vuejs
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/11/19
 * Time: 21.58
 */
@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRespository;

    public List<Product> findAll() {
        return productRespository.findAll();
    }

    public Optional<Product> findById(Long id) {
        return productRespository.findById(id);
    }

    public Product save(Product stock) {
        return productRespository.save(stock);
    }

    public void deleteById(Long id) {
        productRespository.deleteById(id);
    }
}